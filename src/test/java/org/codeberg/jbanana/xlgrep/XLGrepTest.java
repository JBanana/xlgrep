package org.codeberg.jbanana.xlgrep;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class XLGrepTest {
	private static final String NL              = System.getProperty ( "line.separator" );
	private static final String TEST_FILE_PATH  = "target/test-classes/";
	private static final String OLD_FORMAT_NAME = "OldFormat.xls";
	private static final String NEW_FORMAT_NAME = "NewFormat.xlsx";
	private static final String OLD_FORMAT_FILE = TEST_FILE_PATH + OLD_FORMAT_NAME;
	private static final String NEW_FORMAT_FILE = TEST_FILE_PATH + NEW_FORMAT_NAME;

	@BeforeAll
	public static void setUpStatic () {
		System.out.println ( "Setup static" );
	}

	@BeforeEach
	public static void setUpTest () {
		System.out.println ( "Setup test" );
	}

	@Test
	public void testGrep_OldFormat_Text ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "\"Apples\",\"Bananas\",\"Total\"" )
			),
			new XLGrep().grep ( "Banana", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_OldFormat_FormulaValue ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "123,456,579" )
			),
			new XLGrep().grep ( "579", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_OldFormat_ErrorCell ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "\"Got \"\"quotes\"\" in!\",2.5,\"Err: 42\"" )
			),
			new XLGrep().grep ( "Got", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_NewFormat_Text ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				NEW_FORMAT_FILE,
				List.of ( "\"Apples\",\"Bananas\",\"Total\"" )
			),
			new XLGrep().grep ( "Banana", List.of ( NEW_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_NewFormat_FormulaValue ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				NEW_FORMAT_FILE,
				List.of ( "123,456,579" )
			),
			new XLGrep().grep ( "579", List.of ( NEW_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_NewFormat_ErrorCell ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				NEW_FORMAT_FILE,
				List.of ( "\"Got \"\"quotes\"\" in!\",2.5,\"Err: 42\"" )
			),
			new XLGrep().grep ( "Got", List.of ( NEW_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_TwoFiles_Text ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE, List.of ( "\"Apples\",\"Bananas\",\"Total\"" ),
				NEW_FORMAT_FILE, List.of ( "\"Apples\",\"Bananas\",\"Total\"" )
			),
			new XLGrep().grep ( "Banana", List.of ( OLD_FORMAT_FILE, NEW_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_TwoFiles_ActualRegex ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE, List.of (
					"\"Apples\",\"Bananas\",\"Total\"",
					"\"Line\\nbreak!\""
				),
				NEW_FORMAT_FILE, List.of (
					"\"Apples\",\"Bananas\",\"Total\""
				)
			),
			new XLGrep().grep ( "a|k", List.of ( OLD_FORMAT_FILE, NEW_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_LineBreak ()
			throws IOException, XLGrepException {
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "\"Line\\nbreak!\"" )
			),
			new XLGrep().grep ( "break", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_Inverted ()
			throws IOException, XLGrepException {
		final var xlgrep = new XLGrep ();

		xlgrep.setInverted ( true );
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "123,456,579" )
			),
			xlgrep.grep ( "[aeiou]", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_CaseInsensitive ()
			throws IOException, XLGrepException {
		final var xlgrep = new XLGrep ();

		xlgrep.setCaseInsensitive ( true );
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "\"Line\\nbreak!\"" )
			),
			xlgrep.grep ( "BrEaK", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testGrep_Literal ()
			throws IOException, XLGrepException {
		final var xlgrep = new XLGrep ();

		xlgrep.setLiteral ( true );
		assertEquals (
			Map.of (
				OLD_FORMAT_FILE,
				List.of ( "\"Got \"\"quotes\"\" in!\",2.5,\"Err: 42\"" )
			),
			xlgrep.grep ( ".", List.of ( OLD_FORMAT_FILE ) )
		);
	}

	@Test
	public void testMain_OneFile ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "Bananas", OLD_FORMAT_FILE );
			assertEquals( "\"Apples\",\"Bananas\",\"Total\"" + NL, baos.toString() );
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_TwoFiles ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "Bananas", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				OLD_FORMAT_FILE + ": \"Apples\",\"Bananas\",\"Total\"" + NL
					+ NEW_FORMAT_FILE + ": \"Apples\",\"Bananas\",\"Total\"" + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_CaseInsensitive ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-i", "BrEaK", OLD_FORMAT_FILE );
			assertEquals( "\"Line\\nbreak!\"" + NL, baos.toString() );
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_Literal ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-F", ".", OLD_FORMAT_FILE );
			assertEquals( "\"Got \"\"quotes\"\" in!\",2.5,\"Err: 42\"" + NL, baos.toString() );
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_Inverted ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-v", "[aeiou]", OLD_FORMAT_FILE );
			assertEquals( "123,456,579" + NL, baos.toString() );
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_Count_OneFile ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-c", "2", OLD_FORMAT_FILE );
			assertEquals( "3" + NL, baos.toString() );
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_Count_TwoFiles ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-c", "2", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				OLD_FORMAT_FILE + ": 3" + NL
					+ NEW_FORMAT_FILE + ": 3" + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_WithoutFileName ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-h", "break", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				"\"Line\\nbreak!\"" + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_WithFileName ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-H", "break", OLD_FORMAT_FILE );
			assertEquals(
				OLD_FORMAT_FILE + ": " + "\"Line\\nbreak!\"" + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_FilesWithMatch ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-l", "break", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				OLD_FORMAT_FILE + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_FilesWithoutMatch ()
			throws IOException, XLGrepException {
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-L", "break", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				NEW_FORMAT_FILE + NL,
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testMain_Help ()
			throws IOException, XLGrepException {
		// Given
		final ByteArrayOutputStream baos   = new ByteArrayOutputStream ();
		final PrintStream           oldOut = System.out;

		try {
			System.setOut ( new PrintStream ( baos ) );
			XLGrep.main ( "-c", "--help", "2", OLD_FORMAT_FILE, NEW_FORMAT_FILE );
			assertEquals(
				"""
				  --help
				    Show usage and exit.
				  -F, --fixed-strings
				    Interpret search pattern as a fixed string, not a regular expression.
				  -c, --count
				    Suppress normal output; instead print a count of matching rows for each
				    input file. With the -v, --invert-match option, count non-matching lines.
				  -i, --ignore-case
				    Ignore case distinctions in patterns and input data, so that characters
				    that differ only in case match each other.
				  -v, --invert-match
				    Invert the sense of matching, to select non-matching rows.
				  -H, --with-filename
				    Print the file name for each match. This is the default when there is
				    more than one file to search.
				  -h, --no-filename
				    Suppress the prefixing of file names on output. This is the default when
				    there is only one file to search.
				  -l, --files-with-matches
				    Suppress normal output; instead print the name of each input file from
				    which output would normally have been printed. Scanning each input file
				    stops upon first match.
				  -L, --files-without-match
				    Suppress normal output; instead print the name of each input file from
				    which no output would normally have been printed.
				""",
				baos.toString()
			);
		} finally {
			System.setOut ( oldOut );
		}
	}

	@Test
	public void testIsXls () {
		assertTrue  ( XLGrep.isXls ( new File ( OLD_FORMAT_FILE ) ) );
		assertFalse ( XLGrep.isXls ( new File ( NEW_FORMAT_FILE ) ) );
	}

	@Test
	public void testIsXLsx () {
		assertFalse ( XLGrep.isXlsx ( new File ( OLD_FORMAT_FILE ) ) );
		assertTrue  ( XLGrep.isXlsx ( new File ( NEW_FORMAT_FILE ) ) );
	}
}
