package org.codeberg.jbanana.xlgrep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.util.ZipSecureFile;

import static java.util.stream.Collectors.toMap;

public class XLGrep {
	public static void main ( final String... args ) {
		final var argList   = new ArrayList < String > ();
		final var xlGrep    = new XLGrep ();
		/*v*/ var forcePath = ( Boolean ) null;

		for ( final var arg : args ) argList.add ( arg );

		var justCount = false;

		for ( final var argIter = argList.iterator (); argIter.hasNext ();  ) {
			final var arg = argIter.next ();

			switch ( arg ) {
				case "--help":
					System.out.print ( 
						"""
						  --help
						    Show usage and exit.
						  -F, --fixed-strings
						    Interpret search pattern as a fixed string, not a regular expression.
						  -c, --count
						    Suppress normal output; instead print a count of matching rows for each
						    input file. With the -v, --invert-match option, count non-matching lines.
						  -i, --ignore-case
						    Ignore case distinctions in patterns and input data, so that characters
						    that differ only in case match each other.
						  -v, --invert-match
						    Invert the sense of matching, to select non-matching rows.
						  -H, --with-filename
						    Print the file name for each match. This is the default when there is
						    more than one file to search.
						  -h, --no-filename
						    Suppress the prefixing of file names on output. This is the default when
						    there is only one file to search.
						  -l, --files-with-matches
						    Suppress normal output; instead print the name of each input file from
						    which output would normally have been printed. Scanning each input file
						    stops upon first match.
						  -L, --files-without-match
						    Suppress normal output; instead print the name of each input file from
						    which no output would normally have been printed.
						"""
					);
					return;
				case "-F":
				case "--fixed-strings":
					xlGrep.setLiteral ( true );
					argIter.remove ();
					break;
				case "-c":
				case "--count":
					justCount = true;
					argIter.remove ();
					break;
				case "-i":
				case "--ignore-case":
					xlGrep.setCaseInsensitive ( true );
					argIter.remove ();
					break;
				case "-v":
				case "--invert-match":
					xlGrep.inverted = true;
					argIter.remove ();
					break;
				case "-H":
				case "--with-filename":
					forcePath = Boolean.TRUE;
					argIter.remove ();
					break;
				case "-h":
				case "--no-filename":
					forcePath = Boolean.FALSE;
					argIter.remove ();
					break;
				case "-l":
				case "--files-with-matches":
					xlGrep.fileMatch = Boolean.TRUE;
					argIter.remove ();
					break;
				case "-L":
				case "--files-without-match":
					xlGrep.fileMatch = Boolean.FALSE;
					argIter.remove ();
					break;
			}
		}

		switch ( argList.size () )
		{
			case 0:
			case 1:
				System.err.println ( "Not enough parameters - expected regex and at least one file." );
				System.exit ( 3 );
			default:
				final var paths    = argList.subList ( 1, argList.size () );
				final var showPath = null == forcePath ? paths.size () > 1 : forcePath.booleanValue ();

				try {
					ZipSecureFile.setMinInflateRatio ( 0.001 );
					if ( justCount )
						xlGrep.count ( argList.get ( 0 ), paths )
							.entrySet ()
							.stream ()
							.forEach (
								entry -> {
									if ( showPath ) System.out.print ( entry.getKey () + ": " );
									System.out.println ( entry.getValue () );
								}
							);
					else if ( xlGrep.fileMatch != null )
						xlGrep.grep ( argList.get ( 0 ), paths )
							.entrySet ()
							.stream ()
							.forEach (
								entry -> {
									if ( ! entry.getValue ().isEmpty () )
										System.out.println ( entry.getKey () );
								}
							);
					else
						xlGrep.grep ( argList.get ( 0 ), paths )
							.entrySet ()
							.stream ()
							.forEach (
								entry -> {
									entry.getValue ().forEach (
										hit -> {
											if ( showPath ) System.out.print ( entry.getKey () + ": " );
											System.out.println ( hit );
										}
									);
								}
							);
				} catch ( final IOException | XLGrepException x ) {
					System.err.println ( "Error: " + x );
					System.exit ( 4 );
				}
				break;
		}
	}

	private boolean inverted;
	private int     flags;
	private Boolean fileMatch;

	public void setInverted ( final boolean inverted ) {
		this.inverted = inverted;
	}

	public void setCaseInsensitive ( final boolean caseInsensitive ) {
		setFlag ( Pattern.CASE_INSENSITIVE, caseInsensitive );
	}

	public void setLiteral ( final boolean literal ) {
		setFlag ( Pattern.LITERAL, literal );
	}

	private void setFlag ( final int flag, final boolean on ) {
		flags = on
			? flags | flag
			: flags & ~ flag;
	}

	public Map < String, Integer > count ( final String regex, final List < String > paths )
			throws IOException, XLGrepException {
		return grep ( regex, paths )
			.entrySet ()
			.stream ()
			.collect (
				toMap (
					e -> e.getKey (),
					e -> e.getValue ().size ()
				)
			);
	}

	public Map < String, List < String > > grep ( final String regex, final List < String > paths )
			throws IOException, XLGrepException {
		final var result  = new HashMap < String, List < String > > ();
		final var pattern = Pattern.compile ( regex, flags );

		for ( var path : paths ) {
			final var file = new File ( path );

			if ( ! file.exists () )
				throw new FileNotFoundException ( "Could not find file: " + path );

			if ( isXls ( file ) )
				result.put ( path, grepXls ( pattern, file ) );
			else if ( isXlsx ( file ) )
				result.put ( path, grepXlsx ( pattern, file ) );
			else
				throw new XLGrepException ( "Unrecognised file: " + path );
		}

		return result;
	}

	/*package*/ static boolean isXls ( final File file ) {
		if ( ! file.getName ().toLowerCase ().endsWith ( ".xls" ) )
			return false;

		// read some bytes?

		return true;
	}

	/*package*/ static boolean isXlsx ( final File file ) {
		final var fileName = file.getName ().toLowerCase ();

		if ( ! fileName.endsWith ( ".xlsx" ) && ! fileName.endsWith ( ".xlsm" ) )
			return false;

		// read some bytes?

		return true;
	}

	private List < String > grepXls ( final Pattern pattern, final File file )
			throws IOException, XLGrepException {
		try ( final var is = new FileInputStream ( file ) ) {
			return grepWorkbook ( pattern, new HSSFWorkbook ( is ) );
		}
	}

	private List < String > grepXlsx ( final Pattern pattern, final File file )
			throws IOException, XLGrepException {
		try ( final var is = new FileInputStream ( file ) ) {
			return grepWorkbook ( pattern, new XSSFWorkbook ( is ) );
		}
	}

	private List < String > grepWorkbook ( final Pattern pattern, final Workbook workbook )
			throws XLGrepException {
		final var result = new ArrayList < String > ();

		for ( int i = 0, s = workbook.getNumberOfSheets(); i < s; i++ ) {
			final var sheet = workbook.getSheetAt ( i );

			for ( final var row : sheet ) {
				final var outRow = new ArrayList < String > ();

				for ( final var cell : row )
					outRow.add ( readValue ( cell, cell.getCellType (), true ) );

				final var stringRow = String.join ( ",", outRow );
				final var matcher   = pattern.matcher ( stringRow );
				final var found     = matcher.find ();

				if ( Boolean.TRUE.equals ( fileMatch ) && found ) {
					result.add ( stringRow );
					return result;
				}
				if ( Boolean.FALSE.equals ( fileMatch ) && found )
					return result;

				if ( ( ! inverted && found ) || ( inverted && ! found ) )
					result.add ( stringRow );
			}
		}

		return ( Boolean.FALSE.equals ( fileMatch ) && result.isEmpty () )
			? List.of ( "file not matched" )
			: result;
	}

	private static String readValue ( final Cell cell, final CellType cellType, final boolean recurse )
			throws XLGrepException {
		switch ( cellType )
		{
			case NUMERIC:
				return doubleToString ( cell.getNumericCellValue () );
			case ERROR:
				return quote ( "Err: " + cell.getErrorCellValue () );
			case FORMULA:
				if ( ! recurse )
					throw new XLGrepException ( "Unexpected cell type recursion." );
				return readValue ( cell, cell.getCachedFormulaResultType (), false );
			default:
				return quote ( cell.getStringCellValue ().trim () );
		}
	}

	private static String doubleToString ( final double d ) {
		final String s = Double.toString ( d );

		return s.endsWith ( ".0" )
			? s.substring ( 0, s.length () - 2 )
			: s;
	}

	private static String quote ( final String s ) {
		return '"' + s.replace ( "\"", "\"\"" ).replaceAll ( "\\n", "\\\\n" ) + '"';
	}
}
