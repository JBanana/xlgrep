package org.codeberg.jbanana.xlgrep;

public class XLGrepException
		extends Exception {
	public XLGrepException ( final String msg ) {
		super ( msg );
	}
}
