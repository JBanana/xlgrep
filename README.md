# XLGrep
This is a quick hack to allow grepping in Excel spreadsheets.

The regex dialect is the one provided with Java, which is pretty close to other extended regex implementations.

## Usage
```
java -jar xlgrep-0.1-SNAPSHOT-jar-with-dependencies.jar [options] someRegex file1.xls...
```
This will find all the rows in the spreadsheet(s) that match the regex and print them CSV-style.

You can simplify this command by using the sample wrapper scripts - see `bin`

Understood file formats are `.xls` `.xlsx` `.xlsm`

### Options
These options all have the same meaning as for regular grep.
```
  --help
  -F, --fixed-strings
  -c, --count
  -i, --ignore-case
  -v, --invert-match
  -H, --with-filename
  -h, --no-filename
  -l, --files-with-matches
  -L, --files-without-match
```

## Limitations
Regular grep has many options not available here. You can't, for example, use multiple regexes, or choose basic or extended regexes, or all the other things that grep can do.

## Possible future extensions
Besides the many grep options not supported, options that might be useful specifically for spreadsheets might include:
* Allowing cell output instead of rows (maybe with a cell reference prefix)
* Allowing search in formulae (currently only looks at values)
* Other output formats beside CSV

## To build
First `git clone`, then `mvn package`
